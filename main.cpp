/*
    Copyright 2012 Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <QtCore>
#include <QtGui>
#include <QtNetwork>
#include <QtWebKit>

#include <QtDebug>

#include <ui_mainwindow.h>

class AdblockedReply : public QNetworkReply {
    Q_OBJECT
public:
    AdblockedReply(QNetworkAccessManager::Operation op, const QNetworkRequest& request, QObject *parent) : QNetworkReply(parent) {
        setOperation(op);
        setRequest(request);
        setUrl(request.url());
        setError(QNetworkReply::ContentNotFoundError, "adblocked");

        QTimer::singleShot(0, this, SLOT(done()));
    }

    void abort() {}

    qint64 readData(char *data, qint64 maxlen) {
        Q_UNUSED(data)
        Q_UNUSED(maxlen)

        return -1;
    }

public slots:
    void done() {
        emit error(QNetworkReply::ContentNotFoundError);
        emit finished();
    }
};


class AdblockNAM : public QNetworkAccessManager {
public:

    explicit AdblockNAM(QObject *parent = 0) : QNetworkAccessManager(parent) {
        QDir dir(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
        QFile ads(dir.absoluteFilePath("ads.txt"));

        if (!ads.open(QFile::ReadOnly)) {
            qDebug() << ads.fileName() << ads.errorString();
        }

        while (!ads.atEnd()) {
            blockedHosts << ads.readLine().trimmed();
        }

        ads.close();
    }

protected:
    QNetworkReply * createRequest ( Operation op, const QNetworkRequest & req, QIODevice * outgoingData) {
        foreach (const QString& host, blockedHosts) {
            if (req.url().host().contains(host)) {
                qDebug() << "blocked" << req.url();
                return new AdblockedReply(op, req, this);
            }
        }

        return QNetworkAccessManager::createRequest(op, req, outgoingData);
    }

private:
    QStringList blockedHosts;
};

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(bool resetProxy, const QUrl& url = QUrl("http://www.grooveshark.com"), QWidget *parent = 0) : QWidget(parent) {
        ui.setupUi(this);

        QSettings settings("grooveProxy");
        settings.beginGroup("MainWindow");
        resize(settings.value("size", size()).toSize());
        settings.endGroup();

        ui.webView->page()->setNetworkAccessManager(new AdblockNAM(ui.webView->page()));
        ui.webView->load(url);

        if (resetProxy) {
            qDebug() << "*** automatically unset proxy...";
            connect(ui.webView->page(), SIGNAL(loadFinished(bool)), this, SLOT(unsetProxy()));
        }

        ui.proxyHost->setText(QNetworkProxy::applicationProxy().hostName());
        ui.proxyPort->setValue(QNetworkProxy::applicationProxy().port());

        connect(ui.webView, SIGNAL(iconChanged()), this, SLOT(refreshIcon()));
        connect(ui.back, SIGNAL(clicked()), this, SLOT(back()));
        connect(ui.proxyAccept, SIGNAL(clicked()), this, SLOT(switchProxy()));
        connect(ui.proxyClear, SIGNAL(clicked()), this, SLOT(unsetProxy()));
    }

    ~MainWindow() {
        QSettings settings("grooveProxy");
        settings.beginGroup("MainWindow");
        settings.setValue("size", size());
        settings.endGroup();
    }

public slots:
    void unsetProxy() {
        qDebug() << "*** unset Proxy.";
        QNetworkProxy::setApplicationProxy(QNetworkProxy::NoProxy);
        sender()->disconnect(SIGNAL(loadFinished(bool)), this);

        ui.proxyHost->clear();
        ui.proxyPort->setValue(0);
    }

    void switchProxy() {
        if (ui.proxyHost->text().isEmpty() || ui.proxyPort->value() == 0) {
            unsetProxy();
            return;
        }

        QNetworkProxy::setApplicationProxy(QNetworkProxy(QNetworkProxy::HttpProxy, ui.proxyHost->text(), ui.proxyPort->value()));
        qDebug() << "switched proxy to" << ui.proxyHost->text() << ui.proxyPort->value();
    }

    void refreshIcon() {
        setWindowIcon(ui.webView->icon());
    }

    void back() {
        ui.webView->history()->back();
    }

private:
    Ui::MainWindow ui;
};

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    app.setApplicationName("grooveProxy");

    bool resetProxy = false;

    // parse options
    QStringList args = app.arguments();
    args.removeFirst();
    QTextStream qOut(stdout);

    QUrl url("http://www.grooveshark.com");
    bool html5 = false;

    for (QStringList::const_iterator iter = args.begin(); iter != args.end(); iter++) {
        if (*iter == "--proxy" || *iter == "-p") {
            if (++iter == args.end()) {
                qFatal("Missing argument to --proxy");
            }

            QStringList proxyParts = iter->split(':');
            if (proxyParts.length() != 2) {
                qFatal("proxy must be in the form host:port");
            }

            QNetworkProxy::setApplicationProxy(QNetworkProxy(QNetworkProxy::HttpProxy, proxyParts.at(0), proxyParts.at(1).toInt()));

            qOut << "using proxy " << *iter << endl;
        } else if (*iter == "--help" || *iter == "-h") {
            qOut << "Usage: " << argv[0] << " [--proxy|-p host:port] [--reset-proxy|-r] [--html5|-5]" << endl;
            return 0;
        } else if (*iter == "--reset-proxy" || *iter == "-r") {
            resetProxy = true;
        } else if (*iter == "--html5" || *iter == "-5") {
            url = QUrl("http://html5.grooveshark.com");
            html5 = true;
        } else {
            qFatal("unknown option: %s", qPrintable(*iter));
        }
    }

    // enable flash if not using html5
    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, !html5);

    QString cacheLocation = QDesktopServices::storageLocation(QDesktopServices::CacheLocation);
    QDir cache(cacheLocation);
    cache.mkpath("cache");
    QWebSettings::globalSettings()->setIconDatabasePath(cache.absoluteFilePath("cache"));

    MainWindow win(resetProxy, url);
    win.show();

    return app.exec();
}

#include <main.moc>
